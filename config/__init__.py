"""configs"""

import os

from config.default import *

appenv = os.environ.get("APPENV", "dev")

# override default values based on `appenv`
if appenv.lower() == "prod":
    from config.prod import *

if appenv.lower() == "dev":
    from config.dev import *
