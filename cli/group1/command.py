import click
from cli.params import someservice_api_options

from config import *


@click.command()
@someservice_api_options
def subcmd1(someservice_api_username: str, someservice_api_password: str):
    """<Description.>"""
    print(f"APP ENVIRONMENT is {appenv}")
    print(someservice_api_username, someservice_api_password)


@click.command()
@click.option("--name", required=True)
def subcmd2(name: str):
    """<Description.>"""
    print(f"APP ENVIRONMENT is {appenv}")
    print(name)
