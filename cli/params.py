"""common params and shared funcs."""

import sys
import click


class NotRequiredIf(click.Option):
    """This is for handling mutually exclusive arguments
    https://stackoverflow.com/a/44349292
    """

    def __init__(self, *args, **kwargs):
        self.not_required_if = kwargs.pop("not_required_if")
        assert self.not_required_if, "'not_required_if' parameter required"
        kwargs.update({"help": f"mutually exclusive with {self.not_required_if}"})
        super().__init__(*args, **kwargs)

    def handle_parse_result(self, ctx, opts, args):
        """Check for mutual exclusivity of arguments."""
        we_are_present = self.name in opts
        other_present = self.not_required_if in opts

        if other_present and we_are_present:
            raise click.BadArgumentUsage(
                f"{self.name} is mutually exclusive with {self.not_required_if}",
                ctx,
            )

        return super().handle_parse_result(ctx, opts, args)


def _wrap_func(func, options):
    for option in reversed(options):
        func = option(func)
    return func


def someservice_api_options(func):
    """someservice api credentials"""
    options = [
        click.option("--someservice-api-username", required=True),
        click.option(
            "--someservice-api-password",
            prompt=True,
            confirmation_prompt=True,
            hide_input=True,
        ),
    ]
    return _wrap_func(func, list(filter(None, options)))


def another_api_options(func):
    """another api credentials"""
    options = [
        click.option("--another-api-username", required=True),
        click.option(
            "--another-api-password",
            prompt=True,
            confirmation_prompt=True,
            hide_input=True,
        ),
    ]
    return _wrap_func(func, list(filter(None, options)))


def _just_options(func, add_sp_env=True):
    """just creds and other options"""
    options = [
        click.option("--just-username", required=True),
        click.option(
            "--just-authentication",
            type=click.Choice(["LOCAL", "LDAP"]),
            required=True,
        ),
        click.option(
            "--just-password",
            prompt=True,
            confirmation_prompt=True,
            hide_input=True,
        ),
        click.option("--just-sp-env", default="default") if add_sp_env else None,
    ]
    return _wrap_func(func, list(filter(None, options)))


def just_options(func):
    """just creds and other options"""
    return _just_options(func)


def just_options_sans_sp_env(func):
    """just creds and other options"""
    return _just_options(func, add_sp_env=False)


if __name__ == "__main__":
    sys.exit(0)
