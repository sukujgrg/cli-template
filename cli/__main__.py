"""<Description.>"""

import logging
import sys

import click

from .group1 import command as one
from .group2 import command as two


@click.group()
def entry_point():
    """<Description.>"""
    logging.basicConfig(
        stream=sys.stdout,
        level="INFO",
        format="%(levelname)s:%(name)s: %(message)s (%(asctime)s; %(filename)s:%(lineno)d)",
    )


entry_point.add_command(one.subcmd1)
entry_point.add_command(one.subcmd2)
entry_point.add_command(two.subcmd3)

if __name__ == "__main__":
    entry_point(auto_envvar_prefix="BLAH")  # pylint: disable=E1123
