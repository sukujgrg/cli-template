#!/bin/bash
export CONTAINER_NAME=cli
export WORKDIR=/work

bash env.sh > .env

docker run \
        --name ${CONTAINER_NAME} \
        --rm \
        --env-file .env \
        --workdir ${WORKDIR} \
        $(cat .id) \
        "${*}" || exit 1
