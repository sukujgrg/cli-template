# cli arguments as ENV
# https://click.palletsprojects.com/en/8.0.x/options/#values-from-environment-variables

echo BLAH_SUBCMD2_NAME="${ONE_NAME:-test}"
echo BLAH_SUBCMD1_SOMESERVICE_API_USERNAME=${SERVICE_ACCOUNT_USERNAME:-testuser}
echo BLAH_SUBCMD1_SOMESERVICE_API_PASSWORD=${SERVICE_ACCOUNT_PASSWORD:-secret}
