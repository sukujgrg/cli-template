FROM python:3

ADD repo.tar.gz /work 

ADD requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt

WORKDIR /work

ENTRYPOINT ["/bin/bash", "-c"]
