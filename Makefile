.DEFAULT_GOAL := all

local: clean build test
all: local tag release


DOCKER_REGISTRY   := docker.io
DOCKER_REPO       := sukujgrg/cli-template
TS                := $(shell date "+%Y%m%d%H%M%S")
CI_REPO_URL       := $(shell git remote get-url origin)
CI_COMMIT         := $(shell git rev-parse --short HEAD)


_git_status:
	@if [ ! "$(shell git branch --show-current)" = "master" ]; then echo "Not on master branch"; exit 1; fi
	@if [ ! -z "$(shell git status --porcelain)" ]; then echo "Branch is not clean."; exit 1; fi


build:
	tar -czf repo.tar.gz \
			  config cli .pylintrc
	docker build \
		--label org.label-schema.name=$(DOCKER_REPO) \
		--label org.label-schema.vcs-ref=$(CI_COMMIT) \
		--label org.label-schema.vcs-url=$(CI_REPO_URL) \
		--iidfile .id \
		.


clean:
	@-docker rmi -f $(shell cat  .id) > /dev/null 2>&1
	@-rm -f .id


release:
	docker push $(DOCKER_REGISTRY)/$(DOCKER_REPO):$(TS)-$(CI_COMMIT)
	docker push $(DOCKER_REGISTRY)/$(DOCKER_REPO):latest


tag:
	docker tag $(shell cat .id) $(DOCKER_REGISTRY)/$(DOCKER_REPO):$(TS)-$(CI_COMMIT)
	docker tag $(shell cat .id) $(DOCKER_REGISTRY)/$(DOCKER_REPO):latest


test: lint


lint:
	./run.sh pylint **/*.py


smoke-test:
	./run.sh python -m cli --help
