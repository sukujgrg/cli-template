# cli-template

Template for writing a cli application with multiple sub commands.

This template supports configs based on environment variable `APPENV`.

## --help

    $ python3 -m cli --help
    Usage: python -m cli [OPTIONS] COMMAND [ARGS]...

      <Description.>

    Options:
      --help  Show this message and exit.

    Commands:
      subcmd1  <Description.>
      subcmd2  <Description.>
      subcmd3  <Description.>


## make targets

| make target   | description                                   |
| :---          | :---                                          |
| `make build`  | builds a docker image                         |
| `make tag`    | tag docker image with git sha and timestamp   |
| `make release`| push docker image to registry                 |
| `make all`    | equivalent to `make clean build tag release`  |


## pass cli args as environment variable

An environment variable in the format of `PREFIX_COMMAND_VARIABLE` sets the value for corresponding parameter.


    $ export `bash env.sh | xargs`

    $ python3 -m cli subcmd1 --help
    Usage: python -m cli subcmd1 [OPTIONS]

      <Description.>

    Options:
      --someservice-api-username TEXT
                                      [required]
      --someservice-api-password TEXT
      --help                          Show this message and exit.

    $ python3 -m cli subcmd1
    APP ENVIRONMENT is dev
    testuser secret
